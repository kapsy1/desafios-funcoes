filme = {
    'nome': 'Extraordinário',
    'ano_de_lancamento': 2017,
    'diretor': 'Stephen Chbosky',
    'atores': ['Jacob Tremblay', 'Julia Roberts', 'Izabela Vidovic', 'Owen Wilson'],
    'dicionario': {
        'nome': 'Extraordinário',
        'ano_de_lancamento': 2017,
        'diretor': 'Stephen Chbosky',
        'atores': ['Jacob Tremblay', 'Julia Roberts', 'Izabela Vidovic', 'Owen Wilson']
    }
}

print(filme.get('nome'))
print(filme.get('ano_de_lancamento'))
print(filme.get('diretor'))
print(filme.get('atores')[0])
print(filme.get('dicionario').get('atores')[1])